.. |label| replace:: Konfigurierbare Felder in Kaufabwicklung
.. |snippet| replace:: FvOrderAttributes
.. |Author| replace:: 1st Vision GmbH
.. |Entwickler| replace:: Internetfabrik
.. |minVersion| replace:: 5.6.0
.. |maxVersion| replace:: 5.7.7
.. |Version| replace:: 1.1.4
.. |php| replace:: 7.4


|label|
============

.. sectnum::

.. contents:: Inhaltsverzeichnis


Überblick
---------
:Author: |Author|
:PHP: |php|
:Kürzel: |snippet|
:getestet für Shopware-Version: |minVersion| bis |maxVersion|
:Version: |Version|

Beschreibung
------------
Im Backend kann man über einen Formularkonfigurator Felder für die Kaufabwicklung einstellen.

Frontend
--------
Die konfigurierten Felder werden im letzten Schritt vom Checkout unterhalb vom Block "Rechnungs- und Lieferadresse" angezeigt.

.. image:: FvOrderAttributes4.png


Backend
-------
Zuerst werden in der "Freitextfeld-Verwaltung" von Shopware neue Felder in der Tabelle "s_order_attributes" angelegt (bitte alle Optionen aus dem Block "Anzeige Einstellungen" leer lassen). Bitte danach auf "Model generieren" klicken. Es ist aber nicht zwingend erforderlich neue Datenbankfelder anzulegen; bestehende Felder aus der Tabelle "s_order_attributes" können auch verwendet werden.

.. image:: FvOrderAttributes2.png


Unter "Einstellungen" erscheint ein neuer Menüpunkt namens "Felder in Kaufabwicklung". Hier finden Sie die Verwaltung für die neuen Formularfelder.

.. image:: FvOrderAttributes1.png


Folgendes kann in der Eingabemaske konfiguriert werden:

.. image:: FvOrderAttributes3.png

:Ziel-Variable: Feld aus der Tabelle s_order_attributes
:Beschreibung: Label für das Feld; hier wird automatisch ein Textbaustein angelegt, der in der Übersetzungsverwaltung von Shopware geändert bzw. übersetzt werden kann
:Pflichtfeld: Pflichtfeld ja/nein im Frontend
:Reihenfolge: Reihenfolge Anzeige im Frontend
:Shop: Auswahl Subshop
:Nach Versandmethode filtern: Es kann das Feld so konfiguriert werden, dass es nur für eine bestimmte Versandart in der Kaufabwicklung erscheint
:Versandmethode: Wählen Sie hier die Versandart aus, wann das Feld eingeblendet werden soll
:Nach Zahlungsmethode filtern: Es kann das Feld so konfiguriert werden, dass es nur für eine bestimmte Zahlungsart in der Kaufabwicklung erscheint
:Zahlungsmethode: Wählen Sie hier die Zahlungsart aus, wann das Feld eingeblendet werden soll


technische Beschreibung
------------------------

Shop-Datenbank:
_______________
:fv_mapping_order_attributes: Mapping der Formularfelder.


Bei einer Bestellung werden folgende Werte weggeschrieben:
__________________________________________________________
In die Tabelle s_order_attributes (in die ausgewählte Datenbankfelder)


Modifizierte Template-Dateien
-----------------------------
checkout/confirm.tpl


